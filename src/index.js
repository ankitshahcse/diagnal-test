import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

// let store = createStore();

// let routes = () => (<Router>
//     <div>
//         <Route exact path="/" component={App} />
//
//     </div>
// </Router>);

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
