import React from 'react';
import './header.css';
import Autosuggest from 'react-autosuggest';
import Data1 from '../API/CONTENTLISTINGPAGE-PAGE1.json';
import Data2 from '../API/CONTENTLISTINGPAGE-PAGE2.json';
import Data3 from '../API/CONTENTLISTINGPAGE-PAGE3.json';
import _ from 'lodash';

const getSuggestionValue = suggestion => suggestion.name;
const renderSuggestion = suggestion => (
    <div>
        {suggestion.name}
    </div>
);

const theme = {
    container: {
        position: 'relative',
        left: 26,
        color: "#000"
    },
    input: {
        width: "calc(100% - 52px)",
        height: 30,
        padding: '10px 20px',
        fontFamily: 'Helvetica, sans-serif',
        fontWeight: 300,
        fontSize: 16,
        border: '1px solid #aaa',
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
    },
    inputFocused: {
        outline: 'none'
    },
    inputOpen: {
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0
    },
    suggestionsContainer: {
        display: 'none'
    },
    suggestionsContainerOpen: {
        display: 'block',
        position: 'absolute',
        top: 31,
        width: "calc(100% - 52px)",
        border: '1px solid #aaa',
        backgroundColor: '#fff',
        fontFamily: 'Helvetica, sans-serif',
        fontWeight: 300,
        fontSize: 12,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
        zIndex: 2
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },
    suggestion: {
        cursor: 'pointer',
        padding: '10px 20px'
    },
    suggestionHighlighted: {
        backgroundColor: '#ddd'
    }
};

var data = [...Data1.page["content-items"].content, ...Data2.page["content-items"].content, ...Data3.page["content-items"].content];
data = _.uniqBy(data, function (e) {
    return e.name;
});

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showSearchIcon: false,
            suggestions: [],
            value: ''
        }
        this.toggleSearchIcon = this.toggleSearchIcon.bind(this);
        this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
        this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    getSuggestions(value) {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;

        return inputLength === 0 ? [] : data.filter(movie =>
            movie.name.toLowerCase().slice(0, inputLength) === inputValue
        );
    };

    toggleSearchIcon() {
        this.setState({
            showSearchIcon: !this.state.showSearchIcon
        },() => {
            if(!this.state.showSearchIcon) {
                this.props.clearFilterText();
            }
        })
    }

    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };

    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    onFormSubmit(e) {
        e.preventDefault();
        this.props.setFilterText(this.state.value)
    }

    render() {
        if(this.state.showSearchIcon) {

            let {value, suggestions} = this.state;

            const inputProps = {
                placeholder: 'Type a movie name',
                value,
                onChange: this.onChange
            };

            return <div className="header-wrapper">
                <img alt="back" className="header-button" onClick={this.toggleSearchIcon} src={require('../images/Back.png')} />
                <form className="header-text" id="searchForm" onSubmit={this.onFormSubmit}>
                    <Autosuggest
                        suggestions={suggestions}
                        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                        getSuggestionValue={getSuggestionValue}
                        renderSuggestion={renderSuggestion}
                        inputProps={inputProps}
                        theme={theme}
                    />
                </form>
                <button style={{background: "none", border: 0}} type="submit" form="searchForm" >
                    <img alt="search" className="header-button" src={require('../images/search.png')}/>
                </button>
            </div>
        } else {
            return <div className="header-wrapper">
                <h3 className="header-text">Romantic Comedy</h3>
                <img alt="search" className="header-button" onClick={this.toggleSearchIcon} src={require('../images/search.png')}/>
            </div>
        }
    }
}

export default Header;