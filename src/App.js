import React, { Component } from 'react';
import './App.css';
import Header from './header/Header.jsx';
import ListView from './listView/ListView.jsx';
import Data1 from './API/CONTENTLISTINGPAGE-PAGE1.json';
import Data2 from './API/CONTENTLISTINGPAGE-PAGE2.json';
import Data3 from './API/CONTENTLISTINGPAGE-PAGE3.json';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterText: false,
            data: [...Data1.page["content-items"].content, ...Data2.page["content-items"].content, ...Data3.page["content-items"].content]
        }
        this.setFilterText = this.setFilterText.bind(this);
        this.clearFilterText = this.clearFilterText.bind(this);
    }

    setFilterText(value) {
        let data = [...Data1.page["content-items"].content, ...Data2.page["content-items"].content, ...Data3.page["content-items"].content];

        if(value) {
            data = data.filter(movie =>movie.name === value);
        }

        this.setState({
            filterText: value,
            data
        })
    }

    clearFilterText() {
        let data = [...Data1.page["content-items"].content, ...Data2.page["content-items"].content, ...Data3.page["content-items"].content];

        this.setState({
            filterText: false,
            data
        })
    }

    render() {
        return (
              <div className="App">
                <Header setFilterText={this.setFilterText} clearFilterText={this.clearFilterText}/>
                <ListView key={this.state.data.length} data={this.state.data} filterText={this.state.filterText} />
              </div>
        );
    }
}

export default App;
