import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import './listView.css';

class ListView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            movies: [],
            hasMore: true
        }
        this.loadMore = this.loadMore.bind(this)
    }

    paginate(array, page_size, page_number) {
        --page_number;
        return array.slice(page_number * page_size, (page_number + 1) * page_size);
    }

    loadMore(page) {
        let oldContent = [...this.state.movies];
        oldContent.push(...this.paginate(this.props.data, 6, page))

        if(this.state.movies.length >= this.props.data.length) {
            this.setState({
                movies: oldContent,
                hasMore: false,
            });
        } else {
            var self = this;
            this.setState({
                movies: oldContent
            });
        }
    }

    loadImage(content) {
        let image = '';

        try {
            image = require('../images/'+content["poster-image"]);
            return <img alt={content.name} src={image} />
        } catch(e) {
            image = require('../images/placeholder_for_missing_posters.png');
            return <img alt={content.name} src={image} />
        }
    }

    render() {
        let items = [];
        const loader = <div style={{ clear: 'both' }} className="loader">Loading ...</div>;

        this.state.movies.map((content, index) => {

            items.push(<div key={index} className="col-4 list-item" style={{float: "left"}}>
                {this.loadImage(content)}
                <div className="list-text">{content.name}</div>
            </div>);

            return content;

        });
        if(this.props.data.length > 0) {
            return <InfiniteScroll
                pageStart={0}
                loadMore={this.loadMore}
                hasMore={this.state.hasMore}
                loader={loader}>
                <div className="list-wrapper" style={{overflow: "hidden"}}>
                    {items}
                </div>
            </InfiniteScroll>
        } else {
            return <div style={{background: "#171717", color: "#fff", width: "100%", height: "100vh", paddingTop: 100}}>Your Search returns no result.</div>
        }

    }
}

export default ListView;